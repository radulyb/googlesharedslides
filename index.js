var express = require("express")
var app = express();
var server = require("http").Server(app);
var io = require("socket.io").listen(server);

var projectUrl = "https://googlesharedslides-radulybotond175171.codeanyapp.com/";
var googleSlidesUrl = "https://docs.google.com/presentation/d/e/2PACX-1vRk_n03G0cK0-cnykS1iXNRGEU4bkVZYUAKjOXQwU1SeX32eTsi_S5u5AwnIL5dm0ONxPC-k-NCeGOo/embed?start=false&loop=false&delayms=60000&rm=minimal#slide=";

var slidesLength = 48;

/* Static content*/
app.use(express.static('public'));

/* App views */
var handlebars = require("express-handlebars");
app.engine(".hbs", handlebars({
  extname: ".hbs"
}));
app.set("view engine", ".hbs");
app.set("views", "./src/views/");

app.get("/", function(req, res) {
  res.render("screen", {
    googleSlidesUrl: googleSlidesUrl
  });
});

app.get("/projector", function(req, res) {
  res.render("projector", {
    projectUrl: projectUrl,
    slidesLength: slidesLength
  });
});

var currentPageNumber = 1;
var projectorConnected = false;
io.sockets.on("disconnect", function(socket){
  console.log("User disconnected");
})


io.sockets.on("connection", function(socket) {
  socket.join('sapientiaJS');
  
  socket.on("screenConnected", function(data){
    if(projectorConnected){
      io.emit("pageNoChanged", {
        pageNo: currentPageNumber
      });
      projectorConnected = false;
    }
  })
  
  socket.on("projectorConnected", function(data) {
    currentPageNumber = data.pageNo;
    projectorConnected = true;
    io.emit("pageNoChanged", {
      pageNo: currentPageNumber
    });
  });
  
  socket.on("sendMessage", function(data){
    var message = data.message;
    io.in('sapientiaJS').emit("displayMessage", {
      message: message
    })
    
  });
  
  socket.on("changePage", function(data) {
    currentPageNumber = data.pageNo;
    io.in('sapientiaJS').emit("pageNoChanged", {
      pageNo: currentPageNumber
    });
  });
  console.log("User connected");
})


server.listen("3000", function(err) {
  if (!err) {
    console.log("Server is running");
  }
})