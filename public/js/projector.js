var socket;
var pageNumber = 1;

window.onload = function(){
  socket = io.connect();

  socket.emit("projectorConnected", {pageNo: pageNumber});

  socket.on("pageNoChanged", function(data){
    document.getElementById("pageNo").innerHTML = data.pageNo + " / " + slidesLength;
    document.getElementById("controlls").className = "class-room-controlls";
  });      

  document.getElementById("btnNext").addEventListener("click", function(){
    if(pageNumber == slidesLength) return;
    socket.emit("changePage", {pageNo: ++pageNumber});
    document.getElementById("pageNumber").value = pageNumber;
  });
  document.getElementById("btnPrevious").addEventListener("click", function(){
    if(pageNumber == 1) return;
    socket.emit("changePage", {pageNo: --pageNumber});
    document.getElementById("pageNumber").value = pageNumber;
  });
  
  document.getElementById("gotoPage").addEventListener("click", function(){
    var selectedPageNumber = document.getElementById("pageNumber").value;
    if(selectedPageNumber === "" || selectedPageNumber < 1 || selectedPageNumber > slidesLength) return;
    pageNumber = selectedPageNumber;
    socket.emit("changePage", {pageNo: pageNumber});
  });
  
  var sendMessage = function(event){
    if(event && "keyCode" in event && event.keyCode != 13) return;
    
    var message = document.getElementById("message").value;
    
    if(message !== ""){
      document.getElementById("message").value = "";
      socket.emit("sendMessage", {message: message});
    }
    
  }
  
  document.getElementById("message").addEventListener("keydown", sendMessage);
  document.getElementById("sendmessage").addEventListener("click", sendMessage);
}

window.onunload = function(){
  if(socket){
    socket.disconnect();
  }
}