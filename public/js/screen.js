var socket;
var pageNumber = 1;

window.onload = function(){
  socket = io.connect();
  
  socket.emit("screenConnected");
  
  socket.on("pageNoChanged", function(data){
    document.getElementsByTagName("iframe")[0].src= slidesURl + data.pageNo;
    document.getElementById("messagebox").style.display = "none";
  });
  socket.on("displayMessage", function(data){
    var messageBox = document.getElementById("messagebox");
    var urlPattern = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    
    messageBox.innerHTML = data.message.replace(urlPattern, '<a href="$1" target="_blank">Open url</a>');
    messageBox.style.display = "block";
  });
  
  document.getElementById("messagebox").addEventListener("click", function(){
    this.style.display = "none";
  });
}

window.onunload = function(){
  if(socket){
    socket.disconnect();
  }
}